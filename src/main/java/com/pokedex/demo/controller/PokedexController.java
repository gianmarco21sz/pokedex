package com.pokedex.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.pokedex.demo.response.Pokemon;
import com.pokedex.demo.response.PokemonList;
import com.pokedex.demo.response.PokemonSpecies;
import com.pokedex.demo.response.Species;
import com.pokedex.demo.services.PokedexService;

@Controller
public class PokedexController {
	
	@Autowired
	PokedexService pokedexService;
	
	@GetMapping({"/","actualizar"})
	public String inicio(@RequestParam(value = "urlActualizar", defaultValue = "https://pokeapi.co/api/v2/pokemon")String urlActualizar,Model model) {				
		
		String url = urlActualizar;

		PokemonList pokemonList = pokedexService.getAllPokemon(url);
		
		model.addAttribute("count", pokemonList.getCount());
		model.addAttribute("results", pokemonList.getResults());
		model.addAttribute("next",pokemonList.getNext());
		model.addAttribute("previous",pokemonList.getPrevious());
		model.addAttribute("pages",Math.ceil(pokemonList.getCount()/20.0));
		model.addAttribute("contador",-20);
		
		
		return "inicio";
	}	
	
	
	@GetMapping("detallePokemon")
	public String detallePokemon(@RequestParam(value = "idPokemon", defaultValue = "1")String idPokemon,Model model) {		

		Pokemon pokemon = pokedexService.getOnePokemon(idPokemon);
		PokemonSpecies pokemonSpecies = pokedexService.getOnePokemonSpecies(idPokemon);
		List<Species> evoluciones = new ArrayList<Species>();
		if(pokemonSpecies!= null) {
			 evoluciones = pokedexService.getEvolution(pokemonSpecies.getEvolutionChain().getURL());
			
		}
		 model.addAttribute("evoluciones",evoluciones);
		
		
		
		
		model.addAttribute("pokemon",pokemon);
		
		
		
		return "pokemonDetalle";
	}	


}
