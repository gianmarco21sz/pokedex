package com.pokedex.demo.services;

import java.util.List;

import com.pokedex.demo.response.Pokemon;
import com.pokedex.demo.response.PokemonList;
import com.pokedex.demo.response.PokemonSpecies;
import com.pokedex.demo.response.Species;

public interface PokedexService {
	
	public PokemonList getAllPokemon(String url);
	
	public Pokemon getOnePokemon(String url);
	
	public PokemonSpecies getOnePokemonSpecies(String url);
	
	public List<Species> getEvolution(String url);

}
