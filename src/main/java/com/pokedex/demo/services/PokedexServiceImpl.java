package com.pokedex.demo.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.pokedex.demo.response.Chain;
import com.pokedex.demo.response.Evolution;
import com.pokedex.demo.response.Pokemon;
import com.pokedex.demo.response.PokemonList;
import com.pokedex.demo.response.PokemonSpecies;
import com.pokedex.demo.response.Species;

@Service
public class PokedexServiceImpl implements PokedexService{
	
	@Override
	public PokemonList getAllPokemon(String url) {
		
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());					
		
		HttpEntity<?> entity = new HttpEntity<>(null,null);

		ResponseEntity<PokemonList> responseEntity = restTemplate.exchange(url, HttpMethod.GET,entity, PokemonList.class);
		
		return responseEntity.getBody();
	}
	
	@Override
	public Pokemon getOnePokemon(String idPokemon) {
		
		String url = "https://pokeapi.co/api/v2/pokemon/"+idPokemon;		
		
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());					
		
		HttpEntity<?> entity = new HttpEntity<>(null,null);

		ResponseEntity<Pokemon> responseEntity = restTemplate.exchange(url, HttpMethod.GET,entity, Pokemon.class);
		
		return responseEntity.getBody();
	}
	
	@Override
	public PokemonSpecies getOnePokemonSpecies(String idPokemon) {
		String url = "https://pokeapi.co/api/v2/pokemon-species/"+idPokemon;
		
		
		try {
			RestTemplate restTemplate = new RestTemplate();
			restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());					
			
			HttpEntity<?> entity = new HttpEntity<>(null,null);

			ResponseEntity<PokemonSpecies> responseEntity = restTemplate.exchange(url, HttpMethod.GET,entity, PokemonSpecies.class);
			return responseEntity.getBody();
			
		}catch(HttpStatusCodeException ex) {
			return null;
		}
		
		
	}
	
	@Override
	public List<Species> getEvolution(String url) {		
		
	
			RestTemplate restTemplate = new RestTemplate();
			restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());					
			
			HttpEntity<?> entity = new HttpEntity<>(null,null);

			ResponseEntity<Evolution> responseEntity = restTemplate.exchange(url, HttpMethod.GET,entity, Evolution.class);
			
			Evolution evolution = responseEntity.getBody(); 
			
			Chain chain = evolution.getChain();  
			List<Species> speciesList = new ArrayList<Species>();
			
					
			speciesList.add(evolution.getChain().getSpecies());

			
			 	while (!chain.getEvolvesTo().isEmpty()){
					speciesList.add(chain.getEvolvesTo().get(0).getSpecies());
					
					chain = chain.getEvolvesTo().get(0);
					
					
				}
					
			
			
			
			return speciesList;
		
	}

}
