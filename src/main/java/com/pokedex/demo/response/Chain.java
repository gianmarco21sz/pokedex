package com.pokedex.demo.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Chain {
    private List<EvolutionDetail> evolutionDetails;
    private List<Chain> evolvesTo;
    private boolean isBaby;
    private Species species;

    @JsonProperty("evolution_details")
    public List<EvolutionDetail> getEvolutionDetails() { return evolutionDetails; }
    @JsonProperty("evolution_details")
    public void setEvolutionDetails(List<EvolutionDetail> value) { this.evolutionDetails = value; }

    @JsonProperty("evolves_to")
    public List<Chain> getEvolvesTo() { return evolvesTo; }
    @JsonProperty("evolves_to")
    public void setEvolvesTo(List<Chain> value) { this.evolvesTo = value; }

    @JsonProperty("is_baby")
    public boolean getIsBaby() { return isBaby; }
    @JsonProperty("is_baby")
    public void setIsBaby(boolean value) { this.isBaby = value; }

    @JsonProperty("species")
    public Species getSpecies() { return species; }
    @JsonProperty("species")
    public void setSpecies(Species value) { this.species = value; }
}