package com.pokedex.demo.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FlavorTextEntry {
    private String flavorText;
    private Color language;
    private Color version;

    @JsonProperty("flavor_text")
    public String getFlavorText() { return flavorText; }
    @JsonProperty("flavor_text")
    public void setFlavorText(String value) { this.flavorText = value; }

    @JsonProperty("language")
    public Color getLanguage() { return language; }
    @JsonProperty("language")
    public void setLanguage(Color value) { this.language = value; }

    @JsonProperty("version")
    public Color getVersion() { return version; }
    @JsonProperty("version")
    public void setVersion(Color value) { this.version = value; }
}