package com.pokedex.demo.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Name {
    private Color language;
    private String name;

    @JsonProperty("language")
    public Color getLanguage() { return language; }
    @JsonProperty("language")
    public void setLanguage(Color value) { this.language = value; }

    @JsonProperty("name")
    public String getName() { return name; }
    @JsonProperty("name")
    public void setName(String value) { this.name = value; }
}