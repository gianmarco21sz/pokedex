package com.pokedex.demo.response;

public class Other {
    private DreamWorld dreamWorld;
    private Home home;
    private OfficialArtwork officialArtwork;

    public DreamWorld getDreamWorld() { return dreamWorld; }
    public void setDreamWorld(DreamWorld value) { this.dreamWorld = value; }

    public Home getHome() { return home; }
    public void setHome(Home value) { this.home = value; }

    public OfficialArtwork getOfficialArtwork() { return officialArtwork; }
    public void setOfficialArtwork(OfficialArtwork value) { this.officialArtwork = value; }
}
