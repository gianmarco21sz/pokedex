package com.pokedex.demo.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EvolutionChain {
    private String url;

    @JsonProperty("url")
    public String getURL() { return url; }
    @JsonProperty("url")
    public void setURL(String value) { this.url = value; }
}