package com.pokedex.demo.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Variety {
    private boolean isDefault;
    private Color pokemon;

    @JsonProperty("is_default")
    public boolean getIsDefault() { return isDefault; }
    @JsonProperty("is_default")
    public void setIsDefault(boolean value) { this.isDefault = value; }

    @JsonProperty("pokemon")
    public Color getPokemon() { return pokemon; }
    @JsonProperty("pokemon")
    public void setPokemon(Color value) { this.pokemon = value; }
}
