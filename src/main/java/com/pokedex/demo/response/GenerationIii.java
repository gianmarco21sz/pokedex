package com.pokedex.demo.response;

public class GenerationIii {
    private Emerald emerald;
    private Gold fireredLeafgreen;
    private Gold rubySapphire;

    public Emerald getEmerald() { return emerald; }
    public void setEmerald(Emerald value) { this.emerald = value; }

    public Gold getFireredLeafgreen() { return fireredLeafgreen; }
    public void setFireredLeafgreen(Gold value) { this.fireredLeafgreen = value; }

    public Gold getRubySapphire() { return rubySapphire; }
    public void setRubySapphire(Gold value) { this.rubySapphire = value; }
}