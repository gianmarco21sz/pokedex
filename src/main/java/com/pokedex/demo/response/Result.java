package com.pokedex.demo.response;

public class Result {
    private String name;
    private String url;

   
    public String getName() { return  name.substring(0, 1).toUpperCase() + name.substring(1); }
    
    public void setName(String value) { this.name = value; }

  
    public String getURL() { return url; }
  
    public void setURL(String value) { this.url = value; }
}
