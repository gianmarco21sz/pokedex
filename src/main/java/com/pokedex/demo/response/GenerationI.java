package com.pokedex.demo.response;

public class GenerationI {
    private RedBlue redBlue;
    private RedBlue yellow;

    public RedBlue getRedBlue() { return redBlue; }
    public void setRedBlue(RedBlue value) { this.redBlue = value; }

    public RedBlue getYellow() { return yellow; }
    public void setYellow(RedBlue value) { this.yellow = value; }
}