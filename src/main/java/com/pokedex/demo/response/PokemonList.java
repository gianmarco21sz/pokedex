package com.pokedex.demo.response;

import java.util.List;

public class PokemonList {
	 private Double count;
	    private String next;
	    private Object previous;
	    private List<Result> results;

	    public Double getCount() { return count; }
	    public void setCount(Double value) { this.count = value; }

	    public String getNext() { return next; }
	    public void setNext(String value) { this.next = value; }

	    public Object getPrevious() { return previous; }
	    public void setPrevious(Object value) { this.previous = value; }

	    public List<Result> getResults() { return results; }
	    public void setResults(List<Result> value) { this.results = value; }
}
