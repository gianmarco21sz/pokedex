package com.pokedex.demo.response;

public class GenerationVii {
    private DreamWorld icons;
    private Home ultraSunUltraMoon;

    public DreamWorld getIcons() { return icons; }
    public void setIcons(DreamWorld value) { this.icons = value; }

    public Home getUltraSunUltraMoon() { return ultraSunUltraMoon; }
    public void setUltraSunUltraMoon(Home value) { this.ultraSunUltraMoon = value; }
}