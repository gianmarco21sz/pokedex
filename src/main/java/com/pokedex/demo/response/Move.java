package com.pokedex.demo.response;

import java.util.List;

public class Move {
    private Species move;
    private List<VersionGroupDetail> versionGroupDetails;

    public Species getMove() { return move; }
    public void setMove(Species value) { this.move = value; }

    public List<VersionGroupDetail> getVersionGroupDetails() { return versionGroupDetails; }
    public void setVersionGroupDetails(List<VersionGroupDetail> value) { this.versionGroupDetails = value; }
}