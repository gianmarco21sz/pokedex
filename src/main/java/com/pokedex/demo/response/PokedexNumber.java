package com.pokedex.demo.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PokedexNumber {
    private long entryNumber;
    private Color pokedex;

    @JsonProperty("entry_number")
    public long getEntryNumber() { return entryNumber; }
    @JsonProperty("entry_number")
    public void setEntryNumber(long value) { this.entryNumber = value; }

    @JsonProperty("pokedex")
    public Color getPokedex() { return pokedex; }
    @JsonProperty("pokedex")
    public void setPokedex(Color value) { this.pokedex = value; }
}