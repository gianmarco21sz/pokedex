package com.pokedex.demo.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PalParkEncounter {
    private Color area;
    private long baseScore;
    private long rate;

    @JsonProperty("area")
    public Color getArea() { return area; }
    @JsonProperty("area")
    public void setArea(Color value) { this.area = value; }

    @JsonProperty("base_score")
    public long getBaseScore() { return baseScore; }
    @JsonProperty("base_score")
    public void setBaseScore(long value) { this.baseScore = value; }

    @JsonProperty("rate")
    public long getRate() { return rate; }
    @JsonProperty("rate")
    public void setRate(long value) { this.rate = value; }
}