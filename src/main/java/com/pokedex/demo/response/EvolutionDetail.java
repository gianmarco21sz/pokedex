package com.pokedex.demo.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EvolutionDetail {
    private Object gender;
    private Object heldItem;
    private Object item;
    private Object knownMove;
    private Object knownMoveType;
    private Object location;
    private Object minAffection;
    private Object minBeauty;
    private Object minHappiness;
    private long minLevel;
    private boolean needsOverworldRain;
    private Object partySpecies;
    private Object partyType;
    private Object relativePhysicalStats;
    private String timeOfDay;
    private Object tradeSpecies;
    private Species trigger;
    private boolean turnUpsideDown;

    @JsonProperty("gender")
    public Object getGender() { return gender; }
    @JsonProperty("gender")
    public void setGender(Object value) { this.gender = value; }

    @JsonProperty("held_item")
    public Object getHeldItem() { return heldItem; }
    @JsonProperty("held_item")
    public void setHeldItem(Object value) { this.heldItem = value; }

    @JsonProperty("item")
    public Object getItem() { return item; }
    @JsonProperty("item")
    public void setItem(Object value) { this.item = value; }

    @JsonProperty("known_move")
    public Object getKnownMove() { return knownMove; }
    @JsonProperty("known_move")
    public void setKnownMove(Object value) { this.knownMove = value; }

    @JsonProperty("known_move_type")
    public Object getKnownMoveType() { return knownMoveType; }
    @JsonProperty("known_move_type")
    public void setKnownMoveType(Object value) { this.knownMoveType = value; }

    @JsonProperty("location")
    public Object getLocation() { return location; }
    @JsonProperty("location")
    public void setLocation(Object value) { this.location = value; }

    @JsonProperty("min_affection")
    public Object getMinAffection() { return minAffection; }
    @JsonProperty("min_affection")
    public void setMinAffection(Object value) { this.minAffection = value; }

    @JsonProperty("min_beauty")
    public Object getMinBeauty() { return minBeauty; }
    @JsonProperty("min_beauty")
    public void setMinBeauty(Object value) { this.minBeauty = value; }

    @JsonProperty("min_happiness")
    public Object getMinHappiness() { return minHappiness; }
    @JsonProperty("min_happiness")
    public void setMinHappiness(Object value) { this.minHappiness = value; }

    @JsonProperty("min_level")
    public long getMinLevel() { return minLevel; }
    @JsonProperty("min_level")
    public void setMinLevel(long value) { this.minLevel = value; }

    @JsonProperty("needs_overworld_rain")
    public boolean getNeedsOverworldRain() { return needsOverworldRain; }
    @JsonProperty("needs_overworld_rain")
    public void setNeedsOverworldRain(boolean value) { this.needsOverworldRain = value; }

    @JsonProperty("party_species")
    public Object getPartySpecies() { return partySpecies; }
    @JsonProperty("party_species")
    public void setPartySpecies(Object value) { this.partySpecies = value; }

    @JsonProperty("party_type")
    public Object getPartyType() { return partyType; }
    @JsonProperty("party_type")
    public void setPartyType(Object value) { this.partyType = value; }

    @JsonProperty("relative_physical_stats")
    public Object getRelativePhysicalStats() { return relativePhysicalStats; }
    @JsonProperty("relative_physical_stats")
    public void setRelativePhysicalStats(Object value) { this.relativePhysicalStats = value; }

    @JsonProperty("time_of_day")
    public String getTimeOfDay() { return timeOfDay; }
    @JsonProperty("time_of_day")
    public void setTimeOfDay(String value) { this.timeOfDay = value; }

    @JsonProperty("trade_species")
    public Object getTradeSpecies() { return tradeSpecies; }
    @JsonProperty("trade_species")
    public void setTradeSpecies(Object value) { this.tradeSpecies = value; }

    @JsonProperty("trigger")
    public Species getTrigger() { return trigger; }
    @JsonProperty("trigger")
    public void setTrigger(Species value) { this.trigger = value; }

    @JsonProperty("turn_upside_down")
    public boolean getTurnUpsideDown() { return turnUpsideDown; }
    @JsonProperty("turn_upside_down")
    public void setTurnUpsideDown(boolean value) { this.turnUpsideDown = value; }
}