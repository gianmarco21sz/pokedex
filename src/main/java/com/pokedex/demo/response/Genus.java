package com.pokedex.demo.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Genus {
    private String genus;
    private Color language;

    @JsonProperty("genus")
    public String getGenus() { return genus; }
    @JsonProperty("genus")
    public void setGenus(String value) { this.genus = value; }

    @JsonProperty("language")
    public Color getLanguage() { return language; }
    @JsonProperty("language")
    public void setLanguage(Color value) { this.language = value; }
}
