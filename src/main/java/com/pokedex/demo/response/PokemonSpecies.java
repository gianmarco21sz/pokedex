package com.pokedex.demo.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PokemonSpecies {
    private long baseHappiness;
    private long captureRate;
    private Color color;
    private List<Color> eggGroups;
    private EvolutionChain evolutionChain;
    private Object evolvesFromSpecies;
    private List<FlavorTextEntry> flavorTextEntries;
    private List<Object> formDescriptions;
    private boolean formsSwitchable;
    private long genderRate;
    private List<Genus> genera;
    private Color generation;
    private Color growthRate;
    private Color habitat;
    private boolean hasGenderDifferences;
    private long hatchCounter;
    private long id;
    private boolean isBaby;
    private boolean isLegendary;
    private boolean isMythical;
    private String name;
    private List<Name> names;
    private long order;
    private List<PalParkEncounter> palParkEncounters;
    private List<PokedexNumber> pokedexNumbers;
    private Color shape;
    private List<Variety> varieties;

    @JsonProperty("base_happiness")
    public long getBaseHappiness() { return baseHappiness; }
    @JsonProperty("base_happiness")
    public void setBaseHappiness(long value) { this.baseHappiness = value; }

    @JsonProperty("capture_rate")
    public long getCaptureRate() { return captureRate; }
    @JsonProperty("capture_rate")
    public void setCaptureRate(long value) { this.captureRate = value; }

    @JsonProperty("color")
    public Color getColor() { return color; }
    @JsonProperty("color")
    public void setColor(Color value) { this.color = value; }

    @JsonProperty("egg_groups")
    public List<Color> getEggGroups() { return eggGroups; }
    @JsonProperty("egg_groups")
    public void setEggGroups(List<Color> value) { this.eggGroups = value; }

    @JsonProperty("evolution_chain")
    public EvolutionChain getEvolutionChain() { return evolutionChain; }
    @JsonProperty("evolution_chain")
    public void setEvolutionChain(EvolutionChain value) { this.evolutionChain = value; }

    @JsonProperty("evolves_from_species")
    public Object getEvolvesFromSpecies() { return evolvesFromSpecies; }
    @JsonProperty("evolves_from_species")
    public void setEvolvesFromSpecies(Object value) { this.evolvesFromSpecies = value; }

    @JsonProperty("flavor_text_entries")
    public List<FlavorTextEntry> getFlavorTextEntries() { return flavorTextEntries; }
    @JsonProperty("flavor_text_entries")
    public void setFlavorTextEntries(List<FlavorTextEntry> value) { this.flavorTextEntries = value; }

    @JsonProperty("form_descriptions")
    public List<Object> getFormDescriptions() { return formDescriptions; }
    @JsonProperty("form_descriptions")
    public void setFormDescriptions(List<Object> value) { this.formDescriptions = value; }

    @JsonProperty("forms_switchable")
    public boolean getFormsSwitchable() { return formsSwitchable; }
    @JsonProperty("forms_switchable")
    public void setFormsSwitchable(boolean value) { this.formsSwitchable = value; }

    @JsonProperty("gender_rate")
    public long getGenderRate() { return genderRate; }
    @JsonProperty("gender_rate")
    public void setGenderRate(long value) { this.genderRate = value; }

    @JsonProperty("genera")
    public List<Genus> getGenera() { return genera; }
    @JsonProperty("genera")
    public void setGenera(List<Genus> value) { this.genera = value; }

    @JsonProperty("generation")
    public Color getGeneration() { return generation; }
    @JsonProperty("generation")
    public void setGeneration(Color value) { this.generation = value; }

    @JsonProperty("growth_rate")
    public Color getGrowthRate() { return growthRate; }
    @JsonProperty("growth_rate")
    public void setGrowthRate(Color value) { this.growthRate = value; }

    @JsonProperty("habitat")
    public Color getHabitat() { return habitat; }
    @JsonProperty("habitat")
    public void setHabitat(Color value) { this.habitat = value; }

    @JsonProperty("has_gender_differences")
    public boolean getHasGenderDifferences() { return hasGenderDifferences; }
    @JsonProperty("has_gender_differences")
    public void setHasGenderDifferences(boolean value) { this.hasGenderDifferences = value; }

    @JsonProperty("hatch_counter")
    public long getHatchCounter() { return hatchCounter; }
    @JsonProperty("hatch_counter")
    public void setHatchCounter(long value) { this.hatchCounter = value; }

    @JsonProperty("id")
    public long getID() { return id; }
    @JsonProperty("id")
    public void setID(long value) { this.id = value; }

    @JsonProperty("is_baby")
    public boolean getIsBaby() { return isBaby; }
    @JsonProperty("is_baby")
    public void setIsBaby(boolean value) { this.isBaby = value; }

    @JsonProperty("is_legendary")
    public boolean getIsLegendary() { return isLegendary; }
    @JsonProperty("is_legendary")
    public void setIsLegendary(boolean value) { this.isLegendary = value; }

    @JsonProperty("is_mythical")
    public boolean getIsMythical() { return isMythical; }
    @JsonProperty("is_mythical")
    public void setIsMythical(boolean value) { this.isMythical = value; }

    @JsonProperty("name")
    public String getName() { return name; }
    @JsonProperty("name")
    public void setName(String value) { this.name = value; }

    @JsonProperty("names")
    public List<Name> getNames() { return names; }
    @JsonProperty("names")
    public void setNames(List<Name> value) { this.names = value; }

    @JsonProperty("order")
    public long getOrder() { return order; }
    @JsonProperty("order")
    public void setOrder(long value) { this.order = value; }

    @JsonProperty("pal_park_encounters")
    public List<PalParkEncounter> getPalParkEncounters() { return palParkEncounters; }
    @JsonProperty("pal_park_encounters")
    public void setPalParkEncounters(List<PalParkEncounter> value) { this.palParkEncounters = value; }

    @JsonProperty("pokedex_numbers")
    public List<PokedexNumber> getPokedexNumbers() { return pokedexNumbers; }
    @JsonProperty("pokedex_numbers")
    public void setPokedexNumbers(List<PokedexNumber> value) { this.pokedexNumbers = value; }

    @JsonProperty("shape")
    public Color getShape() { return shape; }
    @JsonProperty("shape")
    public void setShape(Color value) { this.shape = value; }

    @JsonProperty("varieties")
    public List<Variety> getVarieties() { return varieties; }
    @JsonProperty("varieties")
    public void setVarieties(List<Variety> value) { this.varieties = value; }
}