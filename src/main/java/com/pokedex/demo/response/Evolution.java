package com.pokedex.demo.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Evolution {
    private Object babyTriggerItem;
    private Chain chain;
    private long id;

    @JsonProperty("baby_trigger_item")
    public Object getBabyTriggerItem() { return babyTriggerItem; }
    @JsonProperty("baby_trigger_item")
    public void setBabyTriggerItem(Object value) { this.babyTriggerItem = value; }

    @JsonProperty("chain")
    public Chain getChain() { return chain; }
    @JsonProperty("chain")
    public void setChain(Chain value) { this.chain = value; }

    @JsonProperty("id")
    public long getID() { return id; }
    @JsonProperty("id")
    public void setID(long value) { this.id = value; }
}
