<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">

<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<link rel="stylesheet" href="resources/css/normalize.css">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link
	href="https://fonts.googleapis.com/css2?family=Kumbh+Sans:wght@400;700&display=swap"
	rel="stylesheet">

<link rel="stylesheet" href="resources/css/estilos.css">
<title>Pokedex</title>
</head>
<body>

	<div class="container-fluid">
		<div class="row col-md-12">
			<c:forEach items="${results}" var="item">


				<div class="col-md-3">
					<article class="card2 m-2">
						<img src="resources/images/bg-pattern-card.svg"
							alt="imagen header card" class="card-header2">
						<div class="card-body2">
							<img
								src="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${item.getURL().split('/')[6]}.png"
								alt="" class="card-body-img2">
							<h1 class="card-body-title2">${item.getName()}</h1>
						</div>
						<div class="card-footer2">
							<div class="card-footer-social2">
								<a
									href="detallePokemon?idPokemon=${item.getURL().split('/')[6]}"
									class="btn btn-block btn-primary">Ver m�s</a>
							</div>
						</div>
					</article>
				</div>


			</c:forEach>
		</div>

		<div class="d-flex justify-content-center align-items-center my-4"
			style="font-size: 15px">
			<div>
				<c:if test="${previous != null}">
					<a href="/actualizar?urlActualizar=${previous}"
						class="btn btn-primary">Previous</a>
				</c:if>

				<c:if test="${previous == null}">
					<button class="btn btn-primary" style="cursor: default;" disabled>Previous</button>
				</c:if>
			</div>


			<div class="mx-3">
				<c:forEach var="i" begin="1" end="${pages}" step="1">
					<a
						href="/actualizar?urlActualizar=https://pokeapi.co/api/v2/pokemon?offset=${contador=contador+20}&limit=20"
						style="margin-right: 7px;">${i}</a>
				</c:forEach>
			</div>


			<div>
				<c:if test="${next != null}">
					<a href="/actualizar?urlActualizar=${next}" class="btn btn-primary">Next</a>
				</c:if>

				<c:if test="${next == null}">
					<button disabled class="btn btn-primary" style="cursor: default;">Next</button>
				</c:if>
			</div>
		</div>
	</div>
</body>
</html>