<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<link rel="stylesheet" href="resources/css/normalize.css">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link
	href="https://fonts.googleapis.com/css2?family=Kumbh+Sans:wght@400;700&display=swap"
	rel="stylesheet">

<link rel="stylesheet" href="resources/css/estilos.css">
<title>Detalle Pokemon</title>
</head>
<body>

	<div class="d-flex justify-content-center mt-3">
	<article class="card2">
		<img src="resources/images/bg-pattern-card.svg"
			alt="imagen header card" class="card-header2">
		<div class="card-body2">
			<img
				src="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${pokemon.getID()}.png"
				alt="" class="card-body-img2">
			<h1 class="card-body-title2">
				${pokemon.getName()} <span>
					${pokemon.getStats().get(0).getBaseStat()} HP</span>
			</h1>
			<p class="card-body-text2">${pokemon.getBaseExperience()}EXP</p>
		</div>
		<div class="card-footer2">
			<div class="card-footer-social2">
				<h3>${pokemon.getStats().get(1).getBaseStat()}</h3>
				<p>Ataque</p>
			</div>
			<div class="card-footer-social2">
				<h3>${pokemon.getStats().get(2).getBaseStat()}</h3>
				<p>Ataque Especial</p>
			</div>
			<div class="card-footer-social2">
				<h3>${pokemon.getStats().get(3).getBaseStat()}</h3>
				<p>Defensa</p>
			</div>
		</div>
	</article>
	</div>
	
	<c:if test="${evoluciones != null}">
		<div class="row col-md-12 d-flex justify-content-center" style="margin-top:150px;">

		<c:forEach items="${evoluciones}" var="item">
			<div class="col-md-3">
				<div class="card-body2">
					<img
						src="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${item.getURL().split('/')[6]}.png"
						alt="" class="card-body-img2">
					<h1 class="card-body-title2">
						${item.getName()} <span>
					</h1>
				</div>
			</div>
		</c:forEach>
	</div>	
	</c:if>
	
</body>
</html>