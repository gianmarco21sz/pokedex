

# PokeApi en Java

![](https://raw.githubusercontent.com/PokeAPI/media/master/logo/pokeapi_256.png)

En este proyecto se consume el api público de https://pokeapi.co para mostrar todos los pokemones y sus respectivas estadísticas que posee este api.


El proyecto muestra una lista de 20 tarjetas de pokemones, cuenta con paginación para poder ver todos los pokemones disponibles en el api, también cuenta con un botón "Ver más" dentro de cada tarjeta para acceder a una nueva vista en la que se muestra algunas estadísticas del Pokémon y su cadena de evolución en caso el Pokémon seleccionado la tenga.

#### Conceptos, Tecnologías, Herramientas aplicados en el proyecto:
* SpringBoot
* JSTL
* RestTemplate
* https://app.quicktype.io (Para la creación de los Response de la api)

#### Requerimientos:
* JDK 1.8 -> https://www.oracle.com/java/technologies/javase/javase8-archive-downloads.html
* Spring Tool Suite -> https://spring.io/tools  O 
Eclipse IDE -> https://www.eclipse.org/downloads/packages/release/2021-06
* Maven 3.6.1


#### Pasos a seguir para ejecución del proyecto
Luego de clonar el proyecto realice lo siguiente:
* Abrir Spring Tool Suite IDE o Eclipse IDE
* Crear un workspace en Spring Tool Suite IDE o Eclipse IDE
* File > Import > Maven > Existing Maven Projects > "Directorio del proyecto"
* Ejecutar proyecto - Spring Tool Suite IDE :  Run As Spring Boot Application
* Ejecutar proyecto - Eclipse IDE :  Run As Java Application


